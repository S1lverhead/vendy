"""
filename: test_wikipedia.py
project: Vendy
Author: Malanik Jan
Email: malanik(dot)jan(at)gmail(dot)com
Description:
Let's play with selenium and Wiki
We've heard that all roads lead to Philosophy.
Write a simple piece of code to prove it.
• Open random article on Wikipedia
• Click the first link in the article until you get to the page about Philosophy
• Count and print out the number of redirects (transitions)
You can use any OOP language. However, you have to use Selenium and Page Object
Pattern (https://martinfowler.com/bliki/PageObject.html,https://github.com/SeleniumHQ/selenium/w
iki/PageObjects ).
"""

import pytest
from pprint import pprint
from selenium.webdriver import Chrome

from pages.pagelist import WikiPageList
from conf.errors import E_SUCCESS, E_NEXTLINKNOTFOUND, E_NOTENOUGHSELECTORS, E_PHILOSOPHYNOTFOUND, E_BODYTITLENOTFOUND


@pytest.fixture
def browser():
    driver = Chrome()
    driver.implicitly_wait(1)
    pages = WikiPageList(browser)
    yield driver
    driver.quit()


@pytest.fixture
def Wiki(browser):
    yield WikiPageList(browser)


def test_get_random_page(Wiki):
    try:
        Wiki.find_randompage()
    except E_SUCCESS:
        assert True
    except (E_NEXTLINKNOTFOUND, E_NOTENOUGHSELECTORS, E_PHILOSOPHYNOTFOUND, E_BODYTITLENOTFOUND) as E:
        pprint(E)
        assert False


def test_find_philosophy(Wiki):
    try:
        Wiki.find_philosophy()
        Wiki.get_results()
    except E_SUCCESS:
        assert True
    except (E_NEXTLINKNOTFOUND, E_NOTENOUGHSELECTORS, E_PHILOSOPHYNOTFOUND, E_BODYTITLENOTFOUND) as E:
        pprint(E)
        assert False

