"""
filename: pagelist.py
project: Vendy
Author: Malanik Jan
Email: malanik(dot)jan(at)gmail(dot)com
Description:
    Let's  create Wiki page list with selectors to find first a href in article.
"""

from pages.page import WikiPage
from conf.selectors import RANDOM_PAGE, TITLE, BODY, LINKS
from conf.conf import ITER, WIKI_HOME
from conf.errors import E_SUCCESS, E_NEXTLINKNOTFOUND, E_NOTENOUGHSELECTORS, E_PHILOSOPHYNOTFOUND, E_BODYTITLENOTFOUND


class WikiPageList:

    def __init__(self, browser):
        self.browser = browser
        self.results = [
#            {"url": "https://en.wikipedia.org/wiki/Wikipedia:WikiProject_Resource_Exchange",
#             "title": ""}
        ]

    def find_randompage(self):
        result = WikiPage.search(self.browser, WIKI_HOME, TITLE, RANDOM_PAGE)
        if isinstance(result, Exception):
            raise result
        self.results.append(result)
        raise E_SUCCESS

    def find_philosophy(self):
        result = {}

        try:
            self.results.append(self.find_randompage())
        except E_SUCCESS:
            pass

        for i in range(ITER):
            dirty = 0
            for link in LINKS:
                print("===================\nLook for link with\n pattern %s\n in url %s" %
                      (link[1], self.results[-1]["url"]))
                try:
                    result = WikiPage.search(self.browser, self.results[-1]["url"], TITLE, link, BODY)
                except E_BODYTITLENOTFOUND:
                    raise E_BODYTITLENOTFOUND
                except E_NEXTLINKNOTFOUND:
                    pass

                if result["url"] == self.results[-1]["url"]:
                    print("Current and last links are similar: \ncurrent: %s\nlast: %s, current selector: %s" %
                          (result["url"], self.results[-1]["url"], link[1]))
                    dirty += 1
                    if dirty == len(LINKS):
                        print("We have problem, current selectors are not sufficient.")
                        raise E_NOTENOUGHSELECTORS
                    continue
                self.results.append(result)
                if "hilosoph" in self.results[-1]["title"]:
                    raise E_SUCCESS
                break
        raise E_PHILOSOPHYNOTFOUND

    def get_results(self):
        print("Results len: %d" % (len(self.results)))
        for result in self.results:
            print("Title: %s\nURL: %s\n" % (result["title"], result["url"]))
