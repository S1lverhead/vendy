"""
filename: page.py
project: Vendy
Author: Malanik Jan
Email: malanik(dot)jan(at)gmail(dot)com
Description:
let's get random page from wiki
temporary returns static web page
"""

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from conf.conf import TIMEOUT
from conf.errors import E_BODYTITLENOTFOUND, E_NEXTLINKNOTFOUND


class WikiPage:
    def __init__(self):
        pass

    @staticmethod
    def get_random_page(browser, url, pattern):
        return WikiPage.__get_title__(browser, url, pattern)

    @staticmethod
    def __get_title__(browser, url, pattern):
        browser.get(url)
        return WebDriverWait(browser, TIMEOUT).until(
            EC.presence_of_element_located(pattern))

    @staticmethod
    def __err_handler__(e, msg, url):
        print("msg: %s\nurl: %s" % (msg, url))

    @staticmethod
    def search(browser, url, title, pattern, root=None):
        try:
            # browser.get(url)
            title = WikiPage.__get_title__(browser, url, pattern)
            if root is not None:
                root = browser.find_element(*root)
        except Exception as e:
            WikiPage.__err_handler__(e, "Error handler\nunable to find BODY or TITLE element.", url)
            raise E_BODYTITLENOTFOUND

        try:
            if root is not None:
                next_link = root.find_element(*pattern)
            else:
                next_link = browser.find_element(*pattern)
        except Exception as e:
            WikiPage.__err_handler__(e, "Error handler\nunable to find NEXT Link.", url)
            raise E_NEXTLINKNOTFOUND

        result = {
            "url": next_link.get_attribute("href"),
            "title": title.get_attribute("innerHTML"),
            "next": next_link
        }
        return result

