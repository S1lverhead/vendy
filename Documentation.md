#Selenium via homebrew
==> Caveats
To have launchd start selenium-server-standalone now and restart at login:
  brew services start selenium-server-standalone
Or, if you don't want/need a background service you can just run:
  selenium-server -port 4444
==> Summary
🍺  /usr/local/Cellar/selenium-server-standalone/3.141.59_1: 5 files, 10.2MB, built in 4 seconds
==> Caveats
==> openjdk
For the system Java wrappers to find this JDK, symlink it with
  sudo ln -sfn /usr/local/opt/openjdk/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk

openjdk is keg-only, which means it was not symlinked into /usr/local,
because it shadows the macOS `java` wrapper.

If you need to have openjdk first in your PATH run:
  echo 'export PATH="/usr/local/opt/openjdk/bin:$PATH"' >> ~/.bash_profile

For compilers to find openjdk you may need to set:
  export CPPFLAGS="-I/usr/local/opt/openjdk/include"

==> selenium-server-standalone
To have launchd start selenium-server-standalone now and restart at login:
  brew services start selenium-server-standalone
Or, if you don't want/need a background service you can just run:
  selenium-server -port 4444
  
  
  
#  chromedriver to path
  05:52:16-janmalanik@Jans-MacBook-Pro-~/Downloads:ls -l chromedriver
-rwxr-xr-x@ 1 janmalanik  staff  14713200 Feb 12 16:47 chromedriver
05:52:18-janmalanik@Jans-MacBook-Pro-~/Downloads:file chromedriver
chromedriver: Mach-O 64-bit executable x86_64
05:52:22-janmalanik@Jans-MacBook-Pro-~/Downloads:mv chromedriver ~/build/
05:52:30-janmalanik@Jans-MacBook-Pro-~/Downloads:echo $PATH
/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
05:52:37-janmalanik@Jans-MacBook-Pro-~/Downloads:mv ~/build/chromedriver /usr/local/bin/


# Notes:
    body = self.browser.find_element(*self.BODY)
    body.get_property("attributes"))
    node() = innerXml
    text() = innerText
    #  "/html/body/div[@id='content']/div[@id='bodyContent']/div[@id='mw-content-text']/div[@class='mw-parser-output']")

# problematic links:
    ihttps://commons.wikimedia.org/wiki/File:Palace_of_king_Zog_durres.jpg#file
