"""
filename: conf/errors.py
project: Vendy
Author: Malanik Jan
Email: malanik(dot)jan(at)gmail(dot)com
Description:
    define custom error return values
"""


class E_SUCCESS(Exception):
    pass


class E_RANDOMPAGE(Exception):
    pass


class E_NOTENOUGHSELECTORS(Exception):
    pass


class E_PHILOSOPHYNOTFOUND(Exception):
    pass


class E_BODYTITLENOTFOUND(Exception):
    pass


class E_NEXTLINKNOTFOUND(Exception):
    pass
