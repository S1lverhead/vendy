"""
filename: conf/selectors.py
project: Vendy
Author: Malanik Jan
Email: malanik(dot)jan(at)gmail(dot)com
Description:
    define selectors for Page Objects
"""

from selenium.webdriver.common.by import By

RANDOM_PAGE = (By.XPATH, ".//a[contains(@href, '/wiki/Special:Random')]")
TITLE = (By.XPATH, "//title")
BODY = (By.XPATH, ".//div[@id='mw-content-text']")
LINKS = ((By.XPATH, ".//a[@class='mw-redirect']"),
         (By.XPATH, ".//a[@href]"))
